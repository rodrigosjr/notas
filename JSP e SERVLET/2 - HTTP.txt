REQUEST / SOLICITA��O
---------------------

- Uma solicita��o HTTP possui:
	- URL, recurso que o cliente esta tentando acessar.
	- Metodos HTTP como get, post, delete, connect, put, head, trace e options. 
	- Dados de parametros de formulario.

- Uma solicita��o Get anexa dados do formulario no final da URL.
- Uma soicita��o Post inclui dados do formulario no corpo da solicita��o.
- O MIME type informa ao bowser que tipo de dados ele dever� receber, assim o bowser saber� como trata-la.


RESPONSE/ RESPOSTA
---------------------

- Uma resposta HTTP possui:
	- C�digo do estatus.
	- Tipo de conteudo (MIME type).
	- Conteudo da resposta (HTML, Imagens, etc)


METODOS HTTP
------------

- Existem 8 metodos HTTP, Get, Post, Head, Trace, Put, Delete, Options, Connect.
- As solicita��es Get aceit�o bookmark, j� a Post n�o.
- O Get deve ser usado apenas para receber coisas. ponto final.
- O Post deve ser usado para enviar coisas.
- Post n�o � um metodo idempotente.
- Idempotente quer dizer que m�ltiplas requisi��es ao mesmo recurso usando um determinado m�todo devem ter o mesmo resultado que teria uma requisi��o apenas.
- Em termos de m�todos de requisi��o HTTP, os m�todos GET, HEAD, PUT e DELETE s�o os que possuem a propriedade de ser idempotentes.
- Se uma requisi��o for concluida e n�o enviar uma resposta ao cliente, pode haver consequencias, onde o cliente pode pensar q a transa��o n�o 
  foi concluida e acabar fazendo uma nova solicita��o obtendo assim um resultado indesejado como por exemplo uma compra duplicada.
- Pode se obter outras "coisas" do objeto solicita��o, como headers, cookies, uma sess�o, a query string e um stream de dados.



