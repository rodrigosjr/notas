Deployment Descriptor
---------------------

- Deployment Descriptor � um arquivo XML que tem como fun��o informar ao container como executar
  seus servlets e JSPs.
- A tag <servlet> mapeia o nome interno para o nome da classe completamente qualificado.
- Entende-se como nome qualificado os diretorios e O nome real da classe.
- A tag <servlet-name> � usado para unir um elemento <servlet> a um elemento especifico <servlet-mapping>
- O nome contido na tag <servlet-name> � de uso interno e invisivel ao usuario final, no html.
- O container utiliza em sua execuss�o o elemento <servlet-mapping>.
- O container utiliza esta informa��o para encontrar o servlet solicitado no request.
- O nome contido no elemento <url-pattern> � utilizado pelo cliente para chegar at� a servlet.
- O nome utilizado pelo cliente � um nome ficticio que faz referencia ao nome real da classe e servelet.
- Exemplo de uma estrutura como a citada acima:


    <servlet>
        <servlet-name>select Beer</servlet-name>
        <servlet-class>classe.Servlet</servlet-class>
    </servlet>

    <servlet-mapping>
        <servlet-name>select Beer</servlet-name>
        <url-pattern>/selectBeer.do</url-pattern>
    </servlet-mapping>


UM POUCO MAIS DE CONTAINER
--------------------------

- o usuario clica em submit.
- O bowser gera a url da solicita��o
- A url � composta pelo nome do diretorio da aplica��o seguido do nome ficticio da servlet.
- Exemplo: /raiz-aplica��o/referencia-servlet.do
- O container procura o DD e encontra um servlet-mapping com um url-pattern que coincida com /referencia-servlet.do, onde / � a raiz.
- O container v� que o servlet-name para esta url-pattern � por exemplo select Beer, onde select Beer n�o � o nome verdadeiro de um
  arquivo de classe servlet e sim o nome dado a um servlet.
- Para o container, servlet � aquilo que foi definido no dd abaixo da tag <servlet>.
- O container procura dentro da tag servlet a tag servlet-name.
- O container usa a servlet-class para descobrir que classe servlet � responsavel por tratar esta solicita��o.
- Se o servlet n�o tiver sido inicializado, a classe � carregada e o servlet � inicializado.
- O container inicia uma nova theard para tratar a solicita��o, e passar a solicita��o para a theard. para o metodo service().
- O container envia a resposta atrav�s do servidor de volta ao cliente.