SingleThreadModel
------------------

- Protege as variaveis de instancia.
- A STM assegura que os servlets lidem apenas com uma solicita��o de cada vez.
- A interface STM n�o possui nenhum m�todo.
- implementar a interface STM assegura que dois encadeamentos n�o ser�o executados simultaneamente no m�todo de servi�o do servlet.
- O container assegura a n�o execuss�o simultanea, sincronizando o acesso a uma unica instancia do servlet ou mantendo um pool de instancias do servlet e enviando
  cada nova solicita��o a um servlet livre.
- A classe servlet deve implementar a interface singleThreadModel. Desta forma o container da web ir� assegurar que este servlet tenha apenas que lidar com
  uma solicita��o de cada vez.


CONTAINER
----------

- O container pode manter um unico servlet, colocar em fila cada solicita��o e processar uma solicita��o completamente antes de permitir que a proxima
  solicita��o prossiga. (Cria-se uma fila de request, e a cada um que � processado um outro vai sendo entregue.)

- No cenario acima existe apenas uma instancia do servlet e nenhuma thread rodando o c�digo desta servlet. 
  (um unico request tem acesso as variaveis de instancia do servlet)

- O container pode criar um pool de instancias do servlet e processar cada solicita��o simultaneamente, uma solicita��o por instancia do servlet.
  (Cada request que chega � encaminhado a uma instancia separada do mesmo servlet livre do pool.)

- No cenario acima existem multiplas instancias de um mesmo servlet, porem nenhum thread de uma mesma instancia. 
  (Cada instancia tem sua variavel, e cada instancia processa apenas um request por vez)

- Utilizando o pool de instancias, caso o servlet possua uma variavel de instancia para contar qtas solicita��es foram atendidas, ele obteria diversas contagens
  diferentes. (cada instancia teria um registro diferente)

- Se o container usar uma estrat�gia de pool, a semantica de algumas variaveis de instancia poder� mudar, uma variavel de instancia q mantem um contador
  dever� ser alterada por exemplo para uma variavel de classe.

- O SingleThreadModel foi desaprovada na API do servlet.



 