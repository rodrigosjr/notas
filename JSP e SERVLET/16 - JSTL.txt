JSTL
-----

- Para utilizar o jstl � preciso declarar a diretiva taglib.
- O atributo "prefix" declara qual o prefixo do Jstl ser� utilizado.
- O atributo "uri" recebe como parametro o core jstl.
- Exemplo de declara��o: <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
- Para utilizar o jstl, antes ser� preciso adicionar a biblioteca stl.jar ao projeto.


<C:OUT >
--------

- <c:out > � uma maneira segura de apresentar as strings fornecidas pelos usuarios, utilize-a no lugar da EL, isso evita o hack com cruzamento de sites.


<C:FOREACH >
------------

- <c:forEach > integra-se perfeitamente com um loop FOR.
- O atributo "var" declara a variavel que armazena cada elemento da cole��o, o seu valor se modifica a cada intera��o.
- O atributo "items" recebe a o objeto (array, map, cole��o) sobre o qual o loop deve ser feito.
- O atributo "varStatus" cria uma nova variavel (Contador, deve-se atribuir um nome) que armazena uma instancia de javax.servlet.jsti.core.loopTagStatus

<C:IF >
--------

- <c:if > inclui uma condicional na pagina HTML.
- O atributo "test" pode receber uma condicional EL para avaliar por exemplo, se um <jsp:include > deve ou n�o ser chamado. <c:if test="${user eq 'menber'}">
- Utilizando o <c:if > n�o � possivel obter um ELSE ou um valor PADR�O caso ambas as alternativas n�o nos atenda.


<C:CHOOSE >
------------

- <c:choose> ajuda a criar multiplas condicionais seguida por um valor padr�o.
- <c:when > deve ser declarada dentro do corpor de <c:choose >, e possui atributos semelhantes a <c:if >, como o atributo "test".
- � preciso declarar um <c:when > para cada condicional.
- <c:otherwise > � utilizado para definir um valor padr�o e tamb�m deve ser declarado dentro do corpo de <c:choose>


<C:SET >
---------

- <c:set > permite-lhe criar/alterar um novo atributo no escopo da solicita��o atrasv�s do atributo "var".
- <c:set > permite-lhe definir valores em um Map ou propriedades de um bean atrav�s do atributo "target".

- O atributo "var" declara um atributo de escopo, e caso este atributo n�o exista no escopo, um novo atributo ser� criado.
- O atributo "scope" define o escopo onde o provavel atributo dever� existir, do contrario um ser� criado.
- Se o atributo "scope" for omitido, o container iniciar� a busca pelo escopo menos abrangente ao mais abrangente.
- O atributo "value" define o valor a ser atribuido na declara��o "var".
- o atributo "value" pode ser omitido e os valores dos atributos podem ser declarados no corpo de <c:set >.
- Suponham que tenhamos o seguinte valor para o atributo "value", Pessoa.Dog(). 
- Se o objeto PESSOA ou DOG for nulo, isso far� com que qualquer atributo do escopo que corresponda com o valor definido em "var" seja apagado.

- O atributo "target" declara um bean ou um map, o parametro de "target" n�o pode ser uma String, e deve ser referenciado por um Objeto (utilize EL).
- O atributo "property" pode referenciar uma propriedade no caso de um bean ou o valor de uma chave no caso de um Map.
- O atributo "value" recebe como parametro o um valor para o atributo.
- Pode-se omitir o atributo "value" e informar no corpo de <c:set > uma String ou uma express�o.


UM POUCO MAIS SOBRE <C:SET >
-----------------------------

- N�o � possivel declarar ambos os atributos "var" e "target".
- "Scope" � opcional, mas, se n�o usa-lo, o padr�o � o page scope.
- Se o "value" for nulo, o atributo nomeado por "var" ser� removido.
- Se o atributo nomeado por "var" n�o existir, ele ser� criado, mas apenas se "value" n�o for nulo.
- Se a express�o "target"for nula, o container gera uma exce��o.
- O "target" serve para inserir uma express�o que resolva para o Objeto Real.
- Se a express�o "target" n�o for um Map, nem um bean, o container gera uma exce��o.
- Se a express�o "target" for um bean, mas este n�o tiver uma propriedade que coincida com "property", o container gera uma exce��o.


<C:REMOVE>
-----------

- <c:remove> remove um atributo do escopo.
- O atributo "var" declara o atributo a ser removido.
- O atributo "scope" declara em qual escopo o atributo a ser removido se encontra.

<C:IMPORT>
------------


- <c:import > � uma terceira alternativa para incluir conteudo na pagina, trata-se de uma alternativa a <jsp:include > e <%@ include >
- O atributo "url" adiciona um conteudo a pagina atual no momento da solicita��o. Funciona da mesma forma que a <jsp:include>, porem � mais flexivel.
- O atributo �rl" permite adicionar um conteudo de fora da aplica��o.
- Podemos customizar o conteudo incluido na pagina atual utilizando a tag <c:param >, basta declara-la no corpo de <c:import >.


<C:PARAM>
----------

- <c:param> � utilizado para acessar os parametros do escopo.
- O atributo "name" referencia o valor da chave do parametro.
- O atributo "value" recebe como parametro o valor que ir� substituir o valor original do parametro do escopo.

<C:URL>
--------

- <c:url> adiciona a jsessionid ao final da url. (Quando estamos trabalhando com Session)
- O atributo "value" recebe como parametro uma url, � ao final desta url que a jsessionid ser� adicionada, isso se os cookies estiverem desabilitados.
- O atributo "var" declara uma variavel que armazena a url.

- Em uma solicita��o get os parametros informados pelo cliente far�o parte da url, e uma url n�o pode conter espa�os. (O espa�o pode vir do parametro do cliente)
- A <c:url> realiza a reescrita de url, mas n�o a codifica��o de url. 
- Como codificar a url utilizando a tag <c:url > evitando assim problemas como os espa�os?
- Basta utilizar a tag <c:param > dentro do corpo de <c:url >, assim os parametros ser�o c�dificados por <c:param > e inseridos ao final da URL.
- 
- 
