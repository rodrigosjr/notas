SESSION ID
-----------

- Na primeira solicita��o do cliente, o container gera uma UNICA session ID, e devolve para o cliente juntamente com a resposta.
- O cliente envia de volta a session ID com cada solicita��o subsequente.
- O container verifica o ID, encontra a sess�o correspondente e a associa � solicita��o.
- Em uma aplica��o distribuida, para cada VM teremos uma c�pia do Servlet, ServletConfig, ServletContext, porem a mesma session ID para a mesma aplica��o
  nunca aparecer� em duas VMs ao mesmo tempo.



CONTAINER / COOKIE
-------------------

- O usuario informa ao container que quer criar ou usar uma sess�o.
- A sess�o � obtida atrav�s do request (solicita��o), pois a sess�o � para o cliente associado aquela solicita��o.
- O Container gera uma sess�o ID, cria um novo objeto cookie, insere a sess�o ID dentro do cookie e configura o cookie como parte da resposta.
- Nas solicita��es subsequentes, o container recebe a session ID de um cookie da solicita��o, compara-a com uma sess�o existente e associa essa sess�o com a 
  solicita��o atual.
- Caso a solicita��o n�o inclua nenhum cookie session ID ou nenhuma sess�o corelacionada for encontrada, o container criar� uma nova session.


SESSION
--------

- O metodo getSession() retorna uma sess�o independente de existir uma sess�o anterior.
- O metodo getSession(FALSE) retorna uma sess�o pr�-existente ou nula, caso n�o haja nenhuma sess�o associada a este cliente.


REESCRITA DE URL
-----------------

- Se o cookie estiver desabilitado no bowser, uma alternativa para obter uma sess�o ID seria a reescrita de URL.
- Em um primeiro momento o container tenta inserir uma sess�o ID no cookie, se este n�o for possivel, o container recorrer� � reescrita de URL.
- A reescrita de URL coloca a sess�o ID no final da URL.
- A reescrita de URL s� ocorrer� se todas as URLs que forem enviadas na resposta estiverem codificadas.
- O container insere na resposta um set-cookie e uma reescrita de URL, assim que o cliente efetua uma nova solicita��o, o servlet chama o request.getSession(),
  o container l� a session ID para a solicita��o, encontra a sess�o, verifica que o cliente aceita cookie e ignora as chamadas response.encodeURL().

- A reescrita de URL � automatica, porem as URLs devem estar codificadas.
- A reescrita de URLs s� podem ser usadas em paginas dinamicas.
- A reescrita influencia na performace, � preciso analisar onde a sess�o � importante para a sua aplica��o.
- A reescrita funciona com o sendRedirect(), a solicita��o � redirecionada para uma URL diferente, e ainda assim usar uma sess�o.
- O metodo em quest�o �: response.encodeRedirectURL("/pagina.do")
- Todas as URLs devem ser rodadas atrav�s do m�todo de um objeto resposta, encodeURL() ou encodeRedirectURL() e o container faz todo o resto.
- A reescrita pode ser usada com JSP.


COOKIE
-------

- O cookie pode ser utilizado para guardar informa��es da sess�o mesmo depois do bowser ser fechado.
- Tudo que se pode fazer com o cookie foi encapsulado na API Servlet em 3 classes: HttpServletRequest, HttpServletResponse e Cookie.
- O cookie desaparece qdo o bowser � fechado, � preciso definir um tempo de vida para que o cookie permanessa vivo. 
- O parametro do metodo setMaxAge esta em segundos cookie.setMaxAge(30*60), � atrav�s deste metodo que definimos o tempo de vida do cookie.


ATRIBUTOS
---------

- Embora tornar os atributos serializaveis n�o seja uma exigencia, � recomendavel o fazer sempre que possivel.
- Atributos serializaveis s�o interessantes em aplica��es distribuidas em mais de uma jvm.
- Atributos session devem ser serializaveis.
- Atributos serializaveis n�o se importam onde s�o colocados, toda vez que uma sess�o migra de uma VM para a outra a serializa��o garante a integridade
  dos atributos.
- Quando um atributo n�o for serializavel ou possuir variaveis de instancia que n�o podem ser serializados, ser� preciso implementar o
  HttpSessionActionListener, que informa qdo uma sess�o esta preste a migrar de uma vm para a outra.
- Um objeto serializavel pode ser salvo e restaurado utilizando-se os seguintes m�todos: readObject() e writeObject().
- HttpSessionActionListener possui metodos semelhantes ao citado acima: sessionDidActivate() e sessionWillPassivate() que auxiliam na migra��o de um atributo.

- Qdo uma classe atributo implementa o HttpSessionBindingListener, o cotainer chama os callbacks que tratam eventos valueBound e valueUnbond qdo uma instancia
  desta classe � adicionada ou removida de uma sess�o.
- Com excess�o do HttpSessionBindingListener, o restante dos listener devem ser registrados no DD.


CONTAR SESS�ES ABERTAS NA APLICA��O
-----------------------------------

public class CountSession implements HttpSessionListener {

   Static private int activeSessions;

   public static int getgActiveSessions(){
        return activeSessions;
   }


   public void sessionCreated(HttpSessionEvent event){
        activeSessions++;
   }


   public void sessionDestroyed(HttpSessionEvent event){
        activeSessions--;
   }

}

- Todos os servlets e classes assistente podem acessar a classe acima.
- A classe deve ser registrada no DD.