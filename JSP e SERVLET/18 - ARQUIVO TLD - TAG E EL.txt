TLD
------

- O TLD descreve duas coisas principais: tags customizadas e fun��es EL.
- A uri pode ser um caminho totalmente qualificado ou um peseudonio definido na tag <uri> do arquivo TLD
- Para instanciar a classe a partir do TLD � preciso extender SimpleTagSupport
- � preciso criar um metodo setter para cada variavel de instancia utilizada como atributo no TLD.

EXEMPLO
--------

<taglib ...>
  <!-- OBRIGATORIO, o desenvolvedor a inclui para declarar a vers�o da biblioteca de tag. -->
  <tlib-version>1.0</tlib-version>
  <!-- OBRIGATORIO, para uso de ferramentas, principalmente... -->
  <short-name>tag-customizadas</short-name>
  <!-- O nome unico que usamos na diretiva taglib. <%@ taglib prefix="m" uri="/WEB-INF/tlds/tag-customizadas.tld" %> -->
  <uri>tag-customizadas</uri>

  <tag>
      <!-- OPCIONAL, mas � uma boa ideia usar. -->
      <description>Objeto Aluno</description>
      <!-- OBRIGATORIO, isto � o que usamos dentro da tag. Exemplo: <m:aluno> -->
      <name>aluno</name>
      <!-- OBRIGATORIO, isto define como o container sabe o que chamar qdo algu�m usa a tag em um JSP. -->
      <tag-class>com.examples.web.Aluno</tag-class>
      <!-- empty - isto diz que a tag N�O pode ter nada no corpo. <m:aluno nome="${param.name}" />  -->
      <!-- JSP - aceita qualquer coisa que possa ser inserida em um JSP -->
      <body-content>empty</body-content> 
      <!-- Se a tag possuir atributos, ent�o � obrigat�rio usar um elemento <attribute> para cada atributo. <m:aluno nome="${param.name}" /> -->
      <attribute>
          <name>nome</name>
          <required>true</required>
          <!-- Isto define que o atributo "user" pode ser um valor de express�o runtime, ou seja, n�o precisa ser uma String literal. <m:aluno nome="${param.name}" /> -->
          <rtexprvalue>true</rtexprvalue>
      </attribute>
      <!-- 
            - O metodo doTag() da classe abstrata SimpleTagSupport nos dispensa da obriga��o de declarar um metodo 
              como ocorre nas fun��es, onde temos que definir o metodo, o tipo dos parametros e o pseudonimo.
              
            - O container chama este m�todo para definir o valor do atributo da tag.
            - Ele usa convers�es de nomea��o javaBean para descobrir que um atributo user deve ser enviado ao metodo setUser().
      -->
  </tag>
</taglib>