JAVAX.SWING
-----------
 
 - O pacote javax.swing contem os componentes graficos em java.
 - Quase tudo que � inserido em uma gui estender� javax.swing.JComponent.


JFRAME
------

 - Um JFrame � o objeto q representa a janela.

 - Todos os elementos da interface s�o inseridos no JFrame.

 - Ao criar um objeto JFrame defina um tamanho para a janela e altere sua visibilidade para true.

 - Utilize o metodo getContentPane().add() para adicionar componentes a moldura.

 - Utilize o metodo setDefaultCloseOperation() juntamente do argumento JFrame.EXIT_ON_CLOSE para encerrar o programa qdo a janela for fechada.


MOLDURA
-------

 - Por padr�o, uma moldura tem cinco regi�es onde os elementos podem ser adicionados.

 - Pode-se adicionar apenas um elemento em cada regi�o da moldura.

 - Este elemento pode ser um painel contendo mais 3 elementos, inclusive outro painel que contenha mais tr�s elementos, e assim sucessivamente.

 - Ao adicionar um elemento � preciso especificar a regi�o (uma constante) e o elemento grafico. 

 - M�todo do JFrame utilizado getContentPane().add(BorderLayout.SOUTH, colorButton). 

EVENTOS
-------

 - Os componentes de GUI do Swing s�o a origem dos eventos.

 - A origem de um evento � um objeto que pode converter a��es do usuario (clique com mouse) em eventos.

 - Um evento � representado como objeto. Um objeto de alguma classe de eventos.

 - A origem de um evento (bot�o, ...) cria um objeto de envento qdo o usuario faz algo relevante, como clicar no bot�o.

 - Cada tipo de evento (WindowsEvents, ...) tem uma interface de escuta correspondente (WindowsListener).

 - A classe precisa implementar uma interface que corresponda ao tipo do evento desejado.

 - Os eventos est�o na interface, portanto deve-se criar um metodo de implementa��o para cada um  dos metodos da interface.


INTERFACE
---------

 - A interface � onde o metodo de retorno de chamada � declarado.

 - Uma interface de escuta � a ponte entre o ouvinte (eu) e a origem do evento (bot�o).

 - Qdo se implementa uma interface de escuta, estar� fornecendo ao bot�o uma maneira de chama-lo.


OUVINTE  DE EVENTOS
-------------------

 - A tarefa do ouvinte � implementar a interface, registrar-se no bot�o e fornecer a manipula��o de eventos.

 - A classe precisa implementar a interface ActionListener para se informar dos ActionEvents de um bot�o.

 - � preciso passar uma referencia do tipo ActionListener como argumento ao bot�o atrav�s do metodo addActionListener(this).

 - como a classe onde o bot�o se localiza implementa uma interface (ela � a interface) passe this como argumento ao metodo referenciado acima.

 - Desta forma qdo o evento ocorrer, o bot�o chamar� o metodo da interface de escuta.

 - Como uma interface ActionListener, a classe precisa implementar seu unico metodo ActionPerformed.

ORIGEM DO EVENTO
----------------

 - A tarefa da origem dos eventos � aceitar registros (dos ouvintes), capturar eventos do usuario e chamar o metodo de manipula��o de eventos do ouvinte.

 - O bot�o � a origem de ActionEvents, portanto ele tem q saber que objetos s�o os ouvintes interessados.

 - O bot�o pegara o parametro recebido, uma referencia do objeto ouvinte, e o armazenar� em uma lista.

 - Desta forma qualquer itera��o (click) com o bot�o acionar� o evento chamado acitionPerformed().

 - Para ser a origem de um evento � preciso:
   - criar um objeto q seja o origem do evento.
   - Fornecer ao objeto um m�todo de registro que come�e com a nomenclatura 'add' e termine com 'listener'.
   - criar uma Interface de escuta para o evento personalizado.
   - Adicionar um ouvinte (Interface) na lista de ouvintes do objeto de origem.
   - Qdo o evento ocorrer, instanciar um objeto Event e envia-lo para o ouvinte de sua lista.
     - O objeto Event cont�m informa��es mais detalhadas do evento.
     - Todo ouvinte possui um metodo abstract a ser implementado, q recebe um argumento do tipo Event.

CLASSES INTERNA
---------------

 - Classe interna � uma classe que reside dentro de outra classe.

 - a classe interna pode acessar as variaveis de instancia e metodos da classe externa.

 - A classe interna pode usar metodos e variaveis da classe externa, mesmo q privados, como se pertencesse a ela mesma.

 - As classe internas s� podem acessar os membros das classes externas a q estejam vinculadas no Heap.

 - As classes externas tb podem acessar os membros de suas classes interna.

 - A classe interna se vincular� a instancia cujo o metodo estiver sendo executado.

 - Para instanciar uma classe interna de fora de uma classe externa, fa�a:
   - Declare uma refer�ncia e vincule-a a um objeto
   - ClasseExterna.ClasseInterna obj = InstanciaObjeto.new ClasseInterna();

 - Classes internas possibilitam q uma interface possa ser implementada mais de 1 vez em uma mesma classe.

 - Desta forma um mesmo metodo de uma interface pode ter diversas implementa��es diferentes, algo q � se encaixe em sobrecarga.

 - Uma classe pode conter 3 bot�es, e � implementar uma interface listener, deixando isso ao encargo das subclasses.

 - Classe internas podem ser usada para fazer com q a classe externa passe no teste �-Um de duas superClasses diferentes.


GERENCIADORES DE LAYOUTS
------------------------

 - Um gerenciador de layout � um objeto java associado a um compenente especifico, quase sempre um componente de plano de fundo.

 - Se uma moldura tiver um painel, e o painel tiver um bot�o, o gerenciador de layout do painel q controlar� o tamanho e inser��o do bot�o.

 - enquanto q o gerenciador de layout da moldura controlar� o tamanho e inser��o do painel.

 - O bot�o, por outro lado, � precisa de um gerenciador de layout, por q � cont�m componentes.

 - O bot�o � sofre interferencia da moldura q contem o painel q por sua vez cont�m o bot�o.

 - Os gerenciadores de layout geralente � respeit�o as especifica��es e atribuem seu proprio criterio de tamanho.


BORDER_LAYOUT
-------------

 - BorderLayout divide um componente plano de fundo em cinco regi�es,ele � o layout padr�o de uma moldura).

 - BorderLayout aceita um componente por regi�o.

 - East e West tem prioridade na altura (o bot�o pode ter a largura q quer).

 - North e south tem prioridade na largura (o bot�o pode ter a altura q quer).

 - Center � tem prioridade e fica com espa�o q sobrar.

 - Use o metodo pack() para usar o centro como ponto inicial, fazendo com q ele fique com o tamanho q desejar em rela��o as outras regi�es.

FLOWER_LAYOUT
-------------

 - Cada componente recebe o tamanho q deseja.

 - Os componentes s�o dispostos da esquerda para a direita na ordem q s�o adicionados.

 - Possui mudan�a de linha automatica, ou seja,  qdo um componente � couber horizontalmente ele passar� para a linha seguinte.

 - FlowLayout da aos componentes seus tamanhos preferidos nas duas dimens�es.

 - FlowLayout � o layout padr�o de um painel.

BOX_LAYOUT
----------

 - BoxLayout tem o mesmo comportamento de FlowLayout, porem pode-se empilhar componentes verticalmente, pois o mesmo � possui mudan�a de linha automatica, podendo for�ar o come�o em uma nova linha.

 - Para alterar o gerenciador de Layout de um painel use o metodo setLayout() e forne�a uma instasncia do novo gerenciador de layout.

 - Exemplo: painel4.setLayout(new BoxLayout(painel4, BoxLayout.Y_AXIS ));

 - Use as variaveis staticas e constantes de BoxLayout para definir o tipo de alinhamento desejado.

SET_LAYOUT
----------

 - Passe um valor nulo para setLayout e defina a posi��o dos componentes manualmente.

 - Bom para Molduras fixas e inviavel para molduras dimensionaveis, existem problemas de posicionamento no redimensionamento.


