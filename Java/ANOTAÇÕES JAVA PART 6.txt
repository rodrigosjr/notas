JAVASOUND
---------

 - Para reproduzir um arquivo MIDi � preciso seguir 4 passos.

 - 1� Ter um Sequenciador para reproduzir a musica.

 - 2� Criar uma Sequencia para representar a musica a ser reproduzida.

 - 3� Obter uma faixa que conter� todas as informa��es do MIDI.

 - 4� O Evento MIDI contem informa��es reais da musica, como notas, instrumentos, dura��o ...


INSTANCIANDO O PLAYER
---------------------

 - Capturar um sequenciador e abri-lo (Sequencer player = MidiSystem.getSequencer()   player.open()).

 - Criar uma nova sequence da musica (Sequence cd = new Sequence(Sequence.PPQ, 4)).

 - Considere os argumentos acima pr�-definido.

 - capturar a faixa da musica em sequence (Track t = cd.createTrack();

 - O objeto track reside em Sequence.

 - Criar um objeto Message (ShortMessage msg = new ShortMessage())

 - Passar as instru��es da MIDI para o Objeto Message ( msg.setMessage(144, 1, 44, 100))

 - Informar ao MidiEvent qdo executar as instru��es da MIDI (MidiEvent noteOn = new MidiEvent(msg, 1))

 - Preencher a faixa com MidiEvent (t.add(noteOn);

 - Os dados MIDI residem em Track.

 - Fornecer a sequence ao sequenciador (player.setSequence(cd);

MIDIEVENT
---------

 - MidiEvent � uma instru��o para parte da can��o.

 - Uma s�rie de MidiEvent � como uma tablatura, informa o q deve ser feito e a hora de faze-lo.

 - O MidiEvent � uma combina��o de mensagens e o momento q devem ser acionadas.

 - As instru��es MIDI s�o inseridas em um Objeto Message.

 - O MidiEvent aciona as instru��es q est�o no Objeto Message no momento certo.

 - O objeto Track armazena todos os MidiEvent.


MENSAGEM MIDI
-------------

 - Uma mensagem midi contem parte do evento q informa o q fazer.

 - O primeiro argumento de uma instru��o � o tipo da mensagem.

 - Uma mensagem 144 significa NOTE ON e 128 significa NOTE OFF.

 - Uma mensagem 192 informa uma mudan�a de instrumento, o terceiro argumento representa o instrumento.

 - O segundo argumento de uma instru��o � o canal (pode se dizer o musico 1).

 - O terceiro argumento de uma instru��o � a nota a reproduzir (0 a 127, notas baixas as altas).

 - O quarto argumento de uma instru��o � a velocidade (intensidade q a nota foi tocada, 100 � um bom padr�o, explore para mais).