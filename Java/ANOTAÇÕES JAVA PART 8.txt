JPanel
------

 - JPanel � um elemento grafico personalizado, o mesmo se refere a uma Interface, q deve implementar uma classe.

 - JPanel � uma superClasse que extende os elementos graficos que ser�o armazenados na janela.

 - JPanel possui um m�todo chamada paintComponent(Graphics g) q deve ser sobreposto.

 - o metodo paintComponet() � pode ser chamado manualmente, ele ser� chamado apenas qdo a janela for desenhada na tela.

 - O argumento Graphics do metodo paintComponet() � fornecido pelo sistema, ele � o espa�o de desenho que sera inserido na tela real.

 - Todos os desenhos s�o gerados dentro do metodo paintComponet().

 - A instancia da classe que extende JPanel deve ser passada ao metodo getContentPane().add(this) da classe JFrame.

 - Para tanto basta usar 'this'.


GRAPHICS
--------

 - Graphics � a SuperClasse de Graphics2D.

 - O parametro do metodo paintComponent(Graphics g) � na verdade uma instancia de Graphics2D.

 - Converta o Objeto Graphics em uma instancia de Graphics2D para ter acesso a outros metodos.