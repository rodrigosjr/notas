METODOS STATIC
--------------

 - Variaveis de instancia e Metodos n�o-estaticos n�o s�o acessiveis a metodos estaticos.

 - Metodos estaticos � podem chamar metodos n�o-estaticos, afinal quem garante q existira uma instancia  desta classe no Heap!

 - O comportamento de um m�todo estatico n�o depende de um valor da variavel de instancia.

 - O metodo estatico atua sobre o argumento, mas nunca � afetado pelo estado de uma variavel de instancia.

 - metodos q nunca usam variaveis de instancia, � depende da existencia de um objeto.

 - Um m�todo estatico deve ser chamado pelo nome da classe.

 - Metodos estaticos podem ser chamados atrav�s de referencias de objetos, porem isso � quer dizer q ele passar� a ter conhecimento do objeto e seus membros.

 - Um metodo estatico pode ser chamado sem q haja qualquer instancia da classe no Heap.

 - Instanciar uma classe q possui apenas metodos estaticos � disperdicio de Heap.

 - Metodos estaticos n�o podem usar o operador this().

 - Uma SuperClasse e todas as suas SubClasses compartilham a mesma c�pia de todos os membros Estaticos.

QUANDO UTILIZAR STATIC
----------------------

 - O fato dos metodos estaticos � utilizarem variaveis de instancia(n�o-estaticas), � � crit�rio para torna-los estaticos.

 - Metodos estaticos s�o recomendados qdo eles � dependam da variavel de instancia e nem venham a depender, ou seja, deixem de ser estaticos para se tornarem metodos n�o-estaticos.

 - Classes q � possuam variaveis de instancia tem fortes indicios para tornarem seus metodos staticos, pois sem variaveis de instancia n�o h� estado e os metodos se comportaram sempre da mesma forma.


VARIAVEIS STATIC
----------------

 - Variaveis staticas s�o compartilhadas com todas as instancias do objeto, assim como os metodos.

 - A variavel estatica esta associada � classe e � � sua instancia, Ou seja, existe uma unica variavel q � compartilhada com todas as instancias.

 - Metodos estaticos podem acessar variaveis de instancia desde q sejam estaticas.

 - Variaveis locais de metodos estaticos n�o s�o variaveis estaticas, ou seja, tem o mesmo comportamento das variaveis locais de motodos n�o-estaticos.

 - N�o � possivel criar uma variavel estatica em um m�todo, sendo ele estatico ou n�o-estatico.

 - variaveis de instancia: 1 por instancia

 - variaveis estaticas: 1 por classe

 - Usar referencia estacica? � criar uma unica instancia da classe?


METODOS �-ESTATICOS
-------------------

 - Metodos n�o-estatico podem acessar variaveis estaticas e fazer chamadas a metodos estacicos.


INICIALIZANDO VARIAVEIS ESTATICAS
---------------------------------

 - variaveis estaticas de uma classe s�o inicializadas antes de qualquer objeto dessa classe possa ser criado.

 - as variaveis estaticas de uma classe s�o inicializadas antes de qualquer m�todo estatico  da classe possa ser executado.

 - Para utilizar uma variavel estatica � preciso carregar a classe a qual pertence.

 - o metodo static { ... } � chamado antes de qualque metodo estatico ser chamado ou variaveis estaticas serem usadas.


IMPORTA��O ESTATICAS
--------------------

 - Ao importar uma classe e definir a importa��o como static, � possivel fazer chamadas aos m�todos estaticos da classe diretamente pelo nome.

 - import static java.lang.Math.* permite chamar um metodo diretamente pelo seu nome sqrt(2.0).


VARIAVEIS FINAL
---------------

 - variaveis marcadas como finais � podem ter seus valores alterados, ou seja, s�o constantes.

 - variaveis finais podem ser publicas para q todos objetos possam acessa-las e estatica para � precisar criar uma instancia da classe a qual pertence.

 - Variaveis constantes por conven��o devem ser escristas em maiusculas e as palavras separadas por under-line.

 - Uma variavel final deve ser inicializada no construtor ou no escopo da declara��o, mesmo sendo uma variavel de instancia ainda assim � preciso inicia-la.

 - para iniciar uma variavel statica final use o metodo static { ... }, porem pode-se atribuir os valores diretamente as variaveis.


OUTRAS UTILIDADE DE FINAL
-------------------------

 - variaveis �-estaticas de instacia e locais incluindo parametros podem ser definidas como finais.

 - O modificador Final impede que metodos sejam sobrepostos.

 - O modificador Final empede que uma classe seja extendida.

 - Se a classe for final os metodos � precisam ser, metodos N�o herdados � correm risco de sobreposi��o.

EMPACOTANDO UM VALOR PRIMITIVO
------------------------------

 - Crie uma instancia da classe correspondente ao tipo primitivo q deseja empacotado (new Integer(i)) e passe como argumento o valor a ser empacotado.

 - Empacotar variaveis do tipo primitivo permite passa-las como argumentos onde objetos do tipo empacotador s�o esperados.

 - Para desempacotar o valor primitivo use o nome da referencia do tipo do empacotador seguido do operador ponto e a express�o Value() correspondente ao tipo primitivo (ex.intValue()).

 - Metodos com argumentos do tipo empacotador (integer) podem receber � s� referencias desse tipo como tb  variaveis primitivas (int).

 - Metodos com argumentos de tipo primitivo (int) podem receber � s� argumentos do tipo primitivo como tb referencias do tipo empacotador (Integer).

 - referencias do tipo empacotador podem ser usadas em conjunto com incremento, decremento, em opera��es matematicas e podem ser atribuidas em tipos primitivos.

 - O compilador sera responsavel por fazer o empacotamente o desempacotamento em cada umas das situa��es citadas acima, Afinal uma referencia � pode ser atribuida em um tipo primitivo.


 - e se atribuir uma referencia do tipo empacotador em um tipo primitivo, sem antes ter empacotado algo?

 - Ocorrer� um erro em tempo de execuss�o, pois um valor null ser� atribuido a variavel do tipo primitiva ocasionando um erro.

 - Qdo uma objeto possui uma variavel de instancia com o mesmo nome de uma variavel local, para q o acesso seja feito na variavel de instancia � precido usar this.nome_da_variavel


IMPORTANTE
----------

 - Blocos estaticos (static{}) s�o executados qdo a classe � carregada, em uma arvore de heran�a o blocos estaticos das superclasses s�o executados primeiro. Em seguida o metodo main � executado e os construtores iniciados partindo das classes mais abstrata as classes mais especificas.